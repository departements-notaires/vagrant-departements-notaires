#!/usr/bin/env bash

set -e

export \
  WWWROOT="/var/www/notaires" \
  DATABASE_DBNAME="notaires" \
  DATABASE_HOST="localhost" \
  DATABASE_USER="notaires" \
  DATABASE_PASSWD="password" \
  DN_MAILER_SENDER_EMAIL="ne-pas-repondre@depnot.departement.fr" \
  DN_REPOS="https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/"
  DN_VERSION="2.0.0-beta.3" \
  DN_BUILD_JOB="37534" \

#https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/jobs/36322/artifacts/raw/build-result/app-2.0.0-beta1-73-g638c151.tgz

# URL of artefact (grabbed from a CI job)
# see https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html#downloading-the-latest-artifacts
# Version and job are tied, grab them from https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/jobs
# https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/jobs/34195/artifacts/raw/build-result/app-2.0.0-beta1.tgz
export \
  DN_ARCHIVE="${DN_REPOS}-/jobs/${DN_BUILD_JOB}/artifacts/raw/build-result/app-${DN_VERSION}.tgz" \
  DN_TGZ="departements-notaires-${DN_VERSION}"

# System pre-requisites
apt-get update
apt-get install -yqq \
  apache2 \
  mariadb-server \
  php-mysql \
  php7.2 \
  php7.2-gd \
  php7.2-intl \
  php7.2-mysql \
  php7.2-xml
a2enmod setenvif rewrite

# Grab the application
mkdir -p "${WWWROOT}"
wget "${DN_ARCHIVE}" -O "${WWWROOT}/${DN_TGZ}"
cd "${WWWROOT}"
tar xvfz "${WWWROOT}/${DN_TGZ}"

# Workaround for https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/3
find "${WWWROOT}" -type d -perm 777  -exec chmod 755 {} \;

# Apache Vhost configuration
cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/000-default-ORIG-SAVE.conf
cat >/etc/apache2/sites-available/000-default.conf <<EOF
<VirtualHost *:80>
    #ServerName example.org
    #ServerAlias www.example.org

    DocumentRoot /var/www/notaires/public
    <Directory /var/www/notaires/public>
        AllowOverride All
        Order Allow,Deny
        Allow from All
    </Directory>

    # uncomment the following lines if you install assets as symlinks
    # or run into problems when compiling LESS/Sass/CoffeeScript assets
    # <Directory /var/www/notaires>
    #     Options FollowSymlinks
    # </Directory>

    ErrorLog /var/log/apache2/notaires_error.log
    CustomLog /var/log/apache2/notaires_access.log combined
</VirtualHost>
EOF
systemctl restart apache2

# Database creation
mysql \
    --execute="GRANT USAGE ON * . * TO '${DATABASE_USER}'@'${DATABASE_HOST}' IDENTIFIED BY '${DATABASE_PASSWD}'; \
        CREATE DATABASE IF NOT EXISTS \`${DATABASE_DBNAME}\` CHARACTER SET utf8mb4; \
        GRANT ALL PRIVILEGES ON \`${DATABASE_DBNAME}\` . * TO '${DATABASE_USER}'@'${DATABASE_HOST}'; \
        FLUSH PRIVILEGES;"

# Configuration file for the application
cp "${WWWROOT}/.env.dist" "${WWWROOT}/.env"
sed -i "s/@database/@${DATABASE_HOST}/g" "${WWWROOT}/.env"
sed -i "s/MAILER_SENDER=/MAILER_SENDER=${DN_MAILER_SENDER_EMAIL}/g" "${WWWROOT}/.env"

# DB app upgrade
"${WWWROOT}/bin/console" -n doctrine:migrations:migrate


# Set permissions
chown -R www-data: "${WWWROOT}"

# End message
echo "Merci. Maintenant, veuillez lancer: ${WWWROOT}/bin/console user:add"
