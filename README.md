# Vagrant-Departements-notaires

Machine virtuelle Vagrant avec installation de Départements & Notaires v2

/!\ Créé à des fins de test uniquement, aucune sécurisation n'a été faite pour une exploitation en production.

## Pré-requis

* Vagrant

## Utilisation

Se placer dans le dossier `DN-v2.0.0/Ubuntu-18.04` puis :

```shell script
vagrant up && vagrant ssh
sudo -i 
cd /vagrant && ./DN.sh
```

L'application est visible sur [http://localhost:8080/](http://localhost:8080/)

Pour détruire la machine : depuis le dossier portant le Vagrantfile lancer `vagrant destroy`
